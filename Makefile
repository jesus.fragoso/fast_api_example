initial-setup:
	pip3 install --upgrade pip
	pip3 install poetry
	poetry init -n
	mkdir .vscode
	touch .vscode/settings.json .vscode/launch.json