#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    CREATE ROLE development_user WITH ENCRYPTED PASSWORD '!4<^T&#ppV%RL';

    ALTER ROLE "development_user" WITH LOGIN;

    ALTER USER development_user WITH SUPERUSER;

    CREATE DATABASE development_user;

    GRANT ALL PRIVILEGES ON DATABASE development_user TO development_user;

    GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to development_user;

    \connect development_user;

    CREATE TABLE users(
        id INTEGER PRIMARY KEY,
        email VARCHAR(250) UNIQUE,
        hashed_password VARCHAR(250),
        is_active BOOLEAN DEFAULT False
    );

    CREATE TABLE cities(
        id INTEGER PRIMARY KEY,
        name VARCHAR(250),
        user_id INTEGER REFERENCES users
    );
EOSQL