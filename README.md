# **FastAPI** project with CI/CD Pipeline.

## Create the Jenkins Docker container. 🐋

<br>

First, run the **Docker** container that is going to run **Jenkins**.

```bash
> docker-compose -f jenkins/docker-compose.yml up -d --build
```

<br>

## Create the GitLab Runner Docker container. 🐋

If you want ot use the **GitLab CI/CD tool** but with a **local custom Runner**, execute the command:

```bash
> docker-compose -f gitlab_compose/docker-compose.yml up -d --build
```

<br>

Then enter the container and execute the following:

```bash
> docker exec -it gitlab-runner-service bash

# Inside the container.
> gitlab-runner register
```

<br>

You will be asked to enter the **GitLab instance URL** where your project lies. It could be the public **GitLab** instance, or a private **GitLab** instance.

![Instance URL](images_docs/instance_url.png)

<br>

Then you will be asked to enter the `registration token`. This can be found inside your project: 
**Settings** -> **CI/CD** -> **Runners** -> **Specific runners**. Copy the registration token showed here and enter it inside the registration flow.

<br>

Then enter a **description** and a **tag** or a **set of tags** for this runner.

![Description and tag](images_docs/runner_description_tags.png)

**Important** 🚨: You need to enter the tag that is defined inside the `.gitlab-ci.yml`.

![GitLab Tags](images_docs/gitlab_tags.png)

<br>

And finally you need to enter a executor, in this case you need to select the **`shell`** executor.

![Select Executor](images_docs/select_executor.png)

Run the tests with the coverage report.

<br>

## Defining the **environment variables** inside `GitLab CI/CD` and inside `Jenkins`.

<br>

- ### For **`GitLab`**:

    If you want to define environment variables inside your project to be used inside the **CI/CD Pipeline**, inside the main page of the project that you have on `GitLab`, go to the **Settings** section, then click on the **CI/CD** option and click on the **`Expand`** button that corresponds to the **`Variables`** section. Here you can create and edit the environment variables that you want to use inside the pipeline of the project.

<br>

- ### For **`Jenkins`**:

    If you are using `Jenkins Pipelines` for the `CI/CD` process, inside the **Jenkins Dashboard** go to the sections **Manage Jenkins** > **Manage Credentials**, and inside this last section, click on the unique **_Store_** listed below, that is called **_Jenkins_**, and finaly click on the unique domain listed, that corresponds to the **_Global Credentials_**. In here you can add and edit the environment variables that you want to use inside the pipeline.

<br>

## Running the tests with **pytest**.
```bash
> pytest --cache-clear -vvv --cov-report=html:htmlcov src/tests
```