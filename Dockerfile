FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9

ENV PYTHONUNBUFFERED=1

WORKDIR /app/

RUN pip3 install --upgrade pip==22.3.1

RUN pip3 install poetry

COPY poetry.lock pyproject.toml /app/

RUN poetry config virtualenvs.create false \
    && poetry install --without dev --no-root

COPY src/ /app/

EXPOSE 7788

EXPOSE 5678

CMD python3 -m debugpy --listen 0.0.0.0:5678 main.py
