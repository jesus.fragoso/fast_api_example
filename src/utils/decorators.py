from functools import wraps
from typing import Callable
from typing import Dict
from typing import NoReturn
from typing import Tuple

from fastapi import Response
from sqlalchemy.exc import IntegrityError

from core.exceptions import SERVICE_EXCEPTIONS


class ResponseHandler:
    @staticmethod
    def custom_response(main_function: Callable) -> Callable:
        @wraps(main_function)
        async def handler_function(*args: Tuple, **kwargs: Dict) -> Dict:
            try:
                result, code = await main_function(*args, **kwargs)
            except tuple(SERVICE_EXCEPTIONS) as exception:
                ResponseHandler._modify_status_code(kwargs["response"], exception.code.value)

                return {
                    "exception_message": exception.message,
                    "code": exception.code,
                }
            except IntegrityError as exception:
                ResponseHandler._modify_status_code(kwargs["response"], 400)

                return {
                    "exception_message": exception._message(),
                    "code": 400,
                }

            return {"data": result, "status_code": code}

        return handler_function

    @staticmethod
    def _modify_status_code(response: Response, status_code: int) -> NoReturn:
        response.status_code = status_code
