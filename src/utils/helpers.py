from pydantic import BaseModel


class UvicornParser(BaseModel):
    """
    Parser that tries to map the arguments
    of the `uvicorn` module to their correct
    datatypes.
    """

    host: str
    port: int
    reload: bool
    log_level: str
    timeout_keep_alive: int
