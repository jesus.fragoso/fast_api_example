from domain.models import User
from domain.repositories import SQLALchemyRepository


class SQLAlchemyAdapter:
    @property
    def user(self):
        return SQLALchemyRepository(User)

    @user.setter
    def user(self, value):
        self._user = value
