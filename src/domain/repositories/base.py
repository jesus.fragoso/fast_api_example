from dataclasses import dataclass
from typing import Dict

from sqlalchemy.orm.session import Session

from domain.client import Base
from domain.client import client_handler


@dataclass
class SQLALchemyRepository:
    model: Base
    session: Session = client_handler.get_session

    def create_entity(self, **data: Dict):
        with self.session() as db_session:
            db_session.add(self.model(**data))
            return db_session.commit()

    def get_entity_by_id(self, entity_id: int):
        with self.session() as db_session:
            return db_session.query(self.model).filter_by(id=entity_id).first()

    def get_all_entities(self):
        with self.session() as db_session:
            return db_session.query(self.model).all()
