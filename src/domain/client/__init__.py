from sqlalchemy.ext.declarative import declarative_base

from .base import ClientHandler

client_handler = ClientHandler()

Base = declarative_base()
