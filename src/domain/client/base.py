from contextlib import contextmanager

from environs import Env
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

env = Env()


class ClientHandler:
    @property
    def engine(self):
        return create_engine(env.str("DATABASE_URL"))

    @property
    def session_factory(self):
        return scoped_session(sessionmaker(**env.dict("SESSION_OPTIONS", subcast_values=bool), bind=self.engine))

    @contextmanager
    def get_session(self):
        session = self.session_factory()

        try:
            yield session
        except Exception as exception:
            session.rollback()
            raise exception
        finally:
            session.close()
