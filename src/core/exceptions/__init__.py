from .users import AllUsersNotFound
from .users import UserNotFound

SERVICE_EXCEPTIONS = [UserNotFound, AllUsersNotFound]
