from core.codes import UserCodes


class UserNotFound(Exception):
    message = "The user wasn't found"
    code = UserCodes.NOT_FOUND


class AllUsersNotFound(Exception):
    message = "There are not users inside the database"
    code = UserCodes.NOT_FOUND
