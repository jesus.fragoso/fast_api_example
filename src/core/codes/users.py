from enum import Enum


class UserCodes(Enum):
    CREATED = 201
    FOUND = 302
    NOT_FOUND = 404
