from domain.adapters import SQLAlchemyAdapter


class BaseCommand:
    adapter = SQLAlchemyAdapter()
