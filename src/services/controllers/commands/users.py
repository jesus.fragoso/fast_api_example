from typing import Dict

from .base import BaseCommand


class UserCommands(BaseCommand):
    def create_user(self, **user_data: Dict):
        return self.adapter.user.create_entity(**user_data)

    def get_user_by_id(self, user_id: int):
        return self.adapter.user.get_entity_by_id(user_id)

    def get_all_users(self):
        return self.adapter.user.get_all_entities()
