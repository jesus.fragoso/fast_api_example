from core.codes import UserCodes
from core.exceptions.users import AllUsersNotFound
from core.exceptions.users import UserNotFound
from domain.entities.users import UserParser

from .commands.users import UserCommands


class UserController:

    commands = UserCommands()

    def create_user(self, **user_data):
        user_instance = UserParser(**user_data)
        self.commands.create_user(**user_instance.dict())
        return user_instance.dict(), UserCodes.CREATED

    def obtain_user(self, user_id):
        user_obtained = self.commands.get_user_by_id(user_id)

        self._verify_user_exist(user_obtained)

        user_instance = UserParser.from_orm(user_obtained)

        return user_instance.dict(), UserCodes.FOUND

    def _verify_user_exist(self, data_obtained):
        if data_obtained is None:
            raise UserNotFound

    def obtain_all_users(self):
        users_obtained = self.commands.get_all_users()

        self._verify_all_users_exist(users_obtained)

        # * Serialize all the users obtained.
        # TODO: Maybe there's a better way.
        all_users_serializer = [UserParser.from_orm(user).dict() for user in users_obtained]

        return all_users_serializer, UserCodes.FOUND

    def _verify_all_users_exist(self, queryset):
        if len(queryset) == 0:
            raise AllUsersNotFound
