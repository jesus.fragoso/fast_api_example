from typing import Dict

from fastapi import Response
from fastapi_utils import inferring_router
from fastapi_utils.cbv import cbv

from services.controllers.users import UserController
from utils.decorators import ResponseHandler

users_router = inferring_router.InferringRouter()

cities_router = inferring_router.InferringRouter()


@cbv(users_router)
class Users:

    controller = UserController()

    @users_router.post("/users", status_code=201)
    @ResponseHandler.custom_response
    async def create_user(self, user_data: Dict, response: Response) -> Dict:
        return self.controller.create_user(**user_data)

    @users_router.get("/users/{user_id}", status_code=302)
    @ResponseHandler.custom_response
    async def get_user(self, user_id: int, response: Response) -> Dict:
        return self.controller.obtain_user(user_id)

    @users_router.get("/users", status_code=302)
    @ResponseHandler.custom_response
    async def get_all_users(self, response: Response) -> Dict:
        return self.controller.obtain_all_users()


@cbv(cities_router)
class Cities:

    controller = ...

    # * TODO: Replace status code constants to enums.
    # * TODO: Maybe create a decorator of this class that could work as
    # * TODO: a general decorator for all the methods inside this class.
    @cities_router.post("/cities", status_code=201)
    @ResponseHandler.custom_response
    async def create_city(self, cities_data: Dict, response: Response) -> Dict:
        pass
