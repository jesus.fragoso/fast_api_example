from typing import Dict
from unittest import mock

from core.codes import UserCodes
from domain.repositories import SQLALchemyRepository
from services.controllers.users import UserController


class TestUsers:
    def test_create_user(self, user_data: Dict):
        with mock.patch.object(SQLALchemyRepository, "create_entity") as create_entity_mock:

            create_entity_mock.return_value = ...

            controller = UserController()
            data, code = controller.create_user(**user_data)

            assert isinstance(data, Dict)
            assert code.value == UserCodes.CREATED.value
            create_entity_mock.assert_called_once()
