from unittest import mock

import pytest
from environs import Env
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session
from sqlalchemy.orm import sessionmaker

from domain.adapters import SQLAlchemyAdapter
from domain.client import Base
from domain.models import User
from domain.repositories import SQLALchemyRepository
from services.controllers.commands.users import UserCommands
from services.controllers.users import UserController

env = Env()


@pytest.fixture(scope="session")
def test_connection():
    engine = create_engine(env.str("DATABASE_URL"))
    return engine.connect()


@pytest.fixture(scope="session")
def database_configuration(test_connection):
    Base.metadata.bind = test_connection
    Base.metadata.create_all()

    # * This `yield` keyword executes all that is before him, when its included as argument of another function.
    # * When the function that includes this fixture ends, the statements that are after the `yield` keyword are executed.
    yield

    Base.metadata.drop_all()


@pytest.fixture(scope="session")
def test_session(database_configuration, test_connection):
    transaction = test_connection.begin()
    yield scoped_session(sessionmaker(**env.dict("SESSION_OPTIONS", subcast_values=bool), bind=test_connection))
    transaction.rollback()


@pytest.fixture(scope="session")
def test_repository(test_session):
    test_repository = SQLALchemyRepository(User)
    test_repository.session = test_session
    yield test_repository


@pytest.fixture(scope="session")
def test_adapter(test_repository):
    with mock.patch.object(SQLAlchemyAdapter, "user", test_repository):
        test_adapter = SQLAlchemyAdapter()
        yield test_adapter


@pytest.fixture(scope="session")
def test_commands(test_adapter):
    with mock.patch.object(UserCommands, "adapter", test_adapter):
        test_commands = UserCommands()
        yield test_commands


@pytest.fixture(scope="session")
def test_controller(test_commands):
    with mock.patch.object(UserController, "commands", test_commands):
        test_controller = UserController()
        yield test_controller


@pytest.fixture(scope="session")
def user_data():
    return {
        "id": 1,
        "email": "test_email@gmail.com",
        "hashed_password": "eHEHXGfzC02CpRNTmJTG",
        "is_active": True,
    }
