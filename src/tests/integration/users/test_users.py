from typing import Dict
from typing import List

from core.codes import UserCodes


class TestUsers:
    def test_create_user(self, test_controller, user_data):
        data, code = test_controller.create_user(**user_data)
        assert isinstance(data, Dict)
        assert code.value == UserCodes.CREATED.value

    def test_get_user(self, test_controller):
        data, code = test_controller.obtain_user(user_id=1)
        assert isinstance(data, Dict)
        assert code.value == UserCodes.FOUND.value

    def test_all_users(self, test_controller):
        data, code = test_controller.obtain_all_users()
        assert isinstance(data, List)
        assert code.value == UserCodes.FOUND.value
