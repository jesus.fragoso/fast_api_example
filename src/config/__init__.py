import os
from dataclasses import dataclass
from typing import NoReturn

from fastapi import FastAPI

import services
from config.development import DevelopmentSettings
from config.production import ProductionSettings
from config.testing import TestingSettings

settings = locals()[os.environ.get("SETTINGS_ENVIRONMENT")]()


@dataclass
class ApplicationHandler:

    app: FastAPI = FastAPI(**settings.APPLICATION_METADATA)

    @classmethod
    def get_application(cls) -> FastAPI:
        cls._adding_middlewares()
        cls._adding_routers()
        return cls.app

    @classmethod
    def _adding_middlewares(cls) -> NoReturn:
        cls.app.add_middleware(*settings.MIDDLEWARE_CLASSES, **settings.MIDDLEWARE_OPTIONS)

    @classmethod
    def _adding_routers(cls) -> NoReturn:
        cls.app.include_router(services.users_router)


app = ApplicationHandler.get_application()
