from typing import Dict
from typing import List

from environs import Env
from starlette.middleware.cors import CORSMiddleware

from utils.helpers import UvicornParser

env = Env()


class DevelopmentSettings:

    APPLICATION_METADATA: Dict = env.dict("APPLICATION_METADATA")

    UVICORN_ARGUMENTS: Dict = UvicornParser(**env.dict("UVICORN_ARGUMENTS")).dict()

    MIDDLEWARE_CLASSES: List = [CORSMiddleware]

    MIDDLEWARE_OPTIONS: Dict = {
        "allow_origins": env.list("ALLOW_ORIGINS", subcast=str),
        "allow_methods": env.list("ALLOW_METHODS", subcast=str),
        "allow_headers": env.list("ALLOW_HEADERS", subcast=str),
        "allow_credentials": env.bool("ALLOW_CREDENTIALS"),
    }
