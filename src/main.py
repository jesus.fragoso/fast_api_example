from uvicorn import run

from config import app
from config import settings

if __name__ == "__main__":
    run("main:app", **settings.UVICORN_ARGUMENTS)
